# Instrucciones - Configurar pixel de publicidad en Tag Manager

Webinar dedicado a la configuración personalizada de los pixel o etiquetas de las plataformas de publicidad en Tag Manager

Veremos las siguientes personalizaciones:
- Etiqueta de Google Ads
- Etiqueta de Bing Ads
- Personalización de Facebook e Instagram Ads
- creación del contenedor de comercio electrónico y de eventos

los ficheros para poder importar en Google Tag Manager:
- para limpiar un contenedor **cleanContainer**, realizar un backup previo
- contenedor utilizado durante el webinar **GTM_webinar_advertising**


## Creación de un nuevo contenedor o limpieza de uno actual (cuidado en borrar los elementos)

Desde la configuración del contendor realizar el paso de limpieza, si es necesario, **cleanContainer** y realiza la operación de limpieza de todas las etiquetas.

Alternativa, crear un nuevo contenedor

## Import de un nuevo contenedor

Desde la configuración del contenedor importar el contenedor **GTM_webinar_advertising** y os pide de reemplazar los ficheros actuales, así tener solo y exclusivamente este contenedor

Pueden utilizar un contenedor como test y realizar las pruebas oportunas y copiar manualmente a vuestro contenedor en producción

## IMPORTANTE 

MEJOR siempre realizar un export del contenedor actual (backup) en caso de importar este (u otro que fuera) para evitar de perder todos los datos.
En todos casos es posible recuperar la información siempre y cuando no se publica el contenedor, pero normalmente se utiliza este como PRUEBA y nunca en PRODUCCIÓN siendo a veces
incompatible con algunos eventos y etiquestas existentes.